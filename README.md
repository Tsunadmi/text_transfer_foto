# Text_transfer_foto_script

Script for transfer code into the photo.
Just for fun and practical.

# Installation

You need install python 3.9:

```
pip install python

```
# Virtual environment

In terminal in project folder you should activate virtual environment:

```
venv/Scripts/activate
```

# Import important libraries

```
Pillow==9.2.0
pytoimage==0.0.1
```

# Getting started

Start of the script:
- Upload cod in a folder of the script
- Text name of Code-file
- Start script

```
python main.py
```

# Technologies used

- Python
- JetBrains

## Description

This script convert code or text into photo.

# Author

This script was done by Dmitry Tsunaev.

- [LinkedIn](https://www.linkedin.com/in/dmitry-tsunaev-530006aa/)
