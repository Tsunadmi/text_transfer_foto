from pytoimage import PyImage
from pathlib import Path


def maker(file_path='main.py'):
    path = Path(file_path)

    if not path.is_file():
        return 'Please, try one more time !!!'

    code = PyImage(file_path, background=(255, 0, 0))
    fone = {
        'line': (0, 255, 255),
        'normal': (0, 0, 0)
    }
    code.set_color_palette(palette=fone)
    code.generate_image()
    img_name = f'{file_path.split(".")[0]}.png'
    code.save_image(img_name)
    return 'Done'


def main():
    file_path = input('Enter filename please: ')
    print(maker(file_path=file_path))


if __name__ == '__main__':
    main()
